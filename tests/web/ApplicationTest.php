<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 30.01.2018
 * Time: 14:28
 */

namespace test\web;

use liw\web\Application;
use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    public function testPlus()
    {
        $app = new Application();
        $this->assertEquals($app->plus(5, 5), 120);
    }

    public function testPlus2()
    {
        $app = new Application();
        $this->assertEquals($app->plus(55, 55), 110);
    }
}
