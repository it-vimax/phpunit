<?php declare(strict_types = 1);

namespace liw\web;

/**
 * Class Application
 *
 * @package liw\web
 */
class Application
{
    public function plus($a, $b)
    {
        return $a + $b;
    }
}
